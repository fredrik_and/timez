package com.sylog.timez.communication;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Scroller;
import android.widget.TextView;

import com.sylog.timez.R;

public class CommTest extends Activity implements OnClickListener, OnMsgRecvIf {

	private static final String TAG = "CommTest";

	private ConnectionHandler connHand = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.comm);

        this.connHand = new ConnectionHandler(this );
        
		((Button) findViewById(R.id.loginButton)).setOnClickListener(this);
		((Button) findViewById(R.id.commButton2)).setOnClickListener(this);
		((Button) findViewById(R.id.commButton3)).setOnClickListener(this);
		((Button) findViewById(R.id.commButton4)).setOnClickListener(this);
		((Button) findViewById(R.id.commSendButton)).setOnClickListener(this);
		((EditText) findViewById(R.id.commandPrompt)).setOnClickListener(this);

		TextView tv = (TextView) findViewById(R.id.replayText);
		tv.setScroller(new Scroller(getApplicationContext()));
		tv.setMaxLines(1000);
		tv.setVerticalScrollBarEnabled(true);
		tv.setMovementMethod(new ScrollingMovementMethod());
		// for (int i = 0; i < 100; i++)
		// tv.append("En rad\n");

//		((TextView)findViewById(R.id.commandPrompt)).invalidate();
//		((TextView)findViewById(R.id.replayText)).invalidate();
		

    }

	public void onClick(View v) {
		Log.i(TAG, "Click");

		if( v.getId() == R.id.loginButton && connHand.isState(ConnectionHandler.IDLE) ){
			
			pusher(CommandFactory.getLoginCommand("android", "android"));
			
			new Timer().scheduleAtFixedRate(new TimerTask() {

				@Override
				public void run() {
					try{
						if( ! connHand.doInBackground() )
							this.cancel();
					}catch(Exception e){
						e.printStackTrace();
					}
				}

			}, new Date(), 100);
			;
		}

		if (!connHand.isState(ConnectionHandler.CONNECTED)) {
			((TextView) findViewById(R.id.commandPrompt))
					.setText("Not connected");
			return;
		}

		switch (v.getId()) {
		case R.id.commButton2:
			pusher(CommandFactory.getProtCommand());
			break;
		case R.id.commButton3:
			pusher(CommandFactory.getListWorIdsCommand());
			break;
		case R.id.commButton4:
			pusher(CommandFactory.getGetWorkCommand(1));
			break;
		case R.id.commSendButton: {
			EditText edT = (EditText) findViewById(R.id.commandPrompt);
			pusher(edT.getText().toString() + "\n");
			int WHITE = 0xffffffff;
			edT.setTextColor(WHITE);

			// To close the keyboard
			InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			mgr.hideSoftInputFromWindow(edT.getWindowToken(), 0);
		}
			break;
		case R.id.commandPrompt: {
			EditText edT = (EditText) findViewById(R.id.commandPrompt);
			edT.selectAll();
			edT.setText("");
			int RED = 0xffff0000;
			edT.setTextColor(RED);
			edT.bringPointIntoView(edT.getText().length());
			InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			// only will trigger it if no physical keyboard is open
			mgr.showSoftInput(edT, InputMethodManager.SHOW_IMPLICIT);

		}
			break;
		default:

			break;
		}

	//	((TextView)findViewById(R.id.commandPrompt)).invalidate();
	//	((TextView)findViewById(R.id.replayText)).invalidate();
	
	
	}

	private void pusher(String str) {
		EditText edT = ((EditText) findViewById(R.id.commandPrompt));
		edT.setText(str);
		int RED = 0xffff0000;
		edT.setTextColor(RED);

		connHand.push(str);
	}

	synchronized public boolean onCallback(final String str) {

		if (str.startsWith("SHOWBIZ")) {
		     runOnUiThread(new Runnable() { 
		            public void run() 
		            { 
					EditText edT = ((EditText) findViewById(R.id.commandPrompt));
					edT.setText(str.replace("SHOWBIZ ", ""));

					int WHITE = 0xffffffff;
					edT.setTextColor(WHITE);
		            } 
		        }); 
		}else{
		     runOnUiThread(new Runnable() { 
		            public void run() 
		            { 
					EditText edT = ((EditText) findViewById(R.id.replayText));
					edT.append(str);

					int WHITE = 0xffffffff;
					edT.setTextColor(WHITE);
		            } 
		        }); 

		}
			
		return true;
	}
}
