package com.sylog.timez.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.util.Log;

public class ConnectionHandler {

	static final int IDLE = 0;
	static final int CONNECTED = 1;
	static final int WORKING = 0;

	private Socket sock = null;
	BufferedReader br = null;
	private static final String TAG = "ConnectionHandler";
	public int state = 0;
	private OnMsgRecvIf cb = null;

	private List<String> writeQ = Collections
			.synchronizedList(new ArrayList<String>());
	private long lastActivity;

	public ConnectionHandler(OnMsgRecvIf cb) {
		this.cb = cb;

	}

	public void push(String str) {
		lastActivity = System.currentTimeMillis();
		writeQ.add(str);
	}

	protected boolean doInBackground() {

		long time = System.currentTimeMillis();
		
		try {

			if (this.sock == null) {
				this.state = WORKING;
				this.connect();
			} else // We are connected
			{
				if (time > (lastActivity + (60 * 1000)))
					throw new TimeOutException(
							"No activity for last 10 seconds");
				send();
				recv();
			}
		} catch (Exception e) {
			if (e instanceof TimeOutException)
				Log.i(TAG, "Timeout");
			try {
				sock.close();
			} catch (IOException e1) {
			}
			sock = null;
			state = IDLE;
			cb.onCallback("SHOWBIZ Disconnect...");
			Log.i(TAG, e.getMessage());
			Log.i(TAG, "Connthread stopping");
			return false;
		}

		return true;
	}

	public void connect() {
		lastActivity = System.currentTimeMillis();

		try {

			if (this.sock != null) {
				this.sock.close();
				writeQ.clear();
				this.sock = null;
			}

			sock = new Socket();
			sock.setSoLinger(true, 1000);
			sock.setSoTimeout(100);
			int timeoutMs = 2000; // 2 seconds
			sock.connect(new InetSocketAddress("jozz.no-ip.org", 4711),
					timeoutMs);
			br = new BufferedReader(
					new InputStreamReader(sock.getInputStream()));
			Log.i(TAG, "Connected to server, Loggin in...");
			this.state = CONNECTED;

		} catch (IOException e) {
			this.sock = null;
			this.state = IDLE;
			e.printStackTrace();

		}

	}

	public void send() throws IOException {

		if (writeQ.isEmpty())
			return;

		for (String str : writeQ) {

			try {
				OutputStreamWriter osw = new OutputStreamWriter(
						this.sock.getOutputStream());
				osw.write(str);
				cb.onCallback("SHOWBIZ " + str);

				osw.flush();

			} catch (IOException e) {
				cb.onCallback("SHOWBIZ ERROR: Could not send command");
				this.sock.close();
				this.state = IDLE;
				throw e;
			}
		}
		writeQ.clear();
	}

	private void recv() throws IOException {
		Log.i(TAG, "recv");

		try {
			while (true) {

				String str = br.readLine();
				if (str != null) {
					lastActivity = System.currentTimeMillis();
					cb.onCallback(str + "\n");
				} else {
					this.sock.close();
					this.sock = null;
					this.state = IDLE;
					cb.onCallback("SHOWBIZ Disconnect...");
					throw new IOException("socket dissconnect");
				}
			}
		} catch (IOException e) {
			if (!(e instanceof SocketTimeoutException))
				throw e;
		}

	}

	public boolean isState(int state) {

		return (this.state == state);
	}

}
