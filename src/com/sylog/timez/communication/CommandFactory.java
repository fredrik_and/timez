package com.sylog.timez.communication;

import java.util.Random;

public class CommandFactory {

	private static Random generator = new Random(19580427);

	private static String getId() {
		return " " + (int) generator.nextInt(999999) + " ";
	}
	
	public static String getLoginCommand(String username, String password){
		String str = "HELO" + getId() + username + " " + password + "\n";
		return str;
	}
	
	public static String getProtCommand(){
	    String str = "PROT\n";
		return str;
	}

	public static String getListWorIdsCommand(){
		String str = "WLST" + getId() + "\n";
		return str;
	}

	public static String getGetWorkCommand(int workId) {
		String str = "WGET" + getId() + workId + " " + "\n";
		return str;
	}

}
