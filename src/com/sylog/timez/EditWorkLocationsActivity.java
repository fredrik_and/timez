package com.sylog.timez;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class EditWorkLocationsActivity extends Activity {

	FileOutputStream outstream = null;
	ObjectInputStream oinstream = null;

	private List<Location> workLocations = new ArrayList<Location>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.editworklocations);
		Bundle b = getIntent().getExtras();

		try {
			oinstream = new ObjectInputStream(new FileInputStream(
					"@string/workLocations"));

			Object obj = null;

			while ((obj = oinstream.readObject()) != null) {
				if (obj instanceof Location) {
					workLocations.add((Location) obj);
				}
			}

		} catch (Exception ex) {
		}

		// ListView savedWorkLocations = (ListView)findViewById(R.id.listView1);

		TextView text = (TextView) findViewById(R.id.location);
		text.setText(String.valueOf(b.getDouble("@string/latitude")) + " : "
				+ String.valueOf(b.getDouble("@string/longitude")));
	}

	public void addWorkLocation(View view) {
		TextView editWorkLocationName = (TextView) findViewById(R.id.workLocationName);
		Location newWorkLocation = new Location(editWorkLocationName.getText()
				.toString());

		workLocations.add(newWorkLocation);
		try {
			outstream = getApplicationContext().openFileOutput(
					"@string/workLocations", Context.MODE_PRIVATE);
			ObjectOutputStream dout = new ObjectOutputStream(outstream);
			dout.writeObject(workLocations);
			dout.flush();
			outstream.getFD().sync();
			outstream.close();
		} catch (IOException e) {
		}
	}
}
