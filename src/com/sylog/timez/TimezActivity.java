package com.sylog.timez;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.sylog.timez.communication.CommTest;

public class TimezActivity extends Activity {

	private Location currentLocation;
	private List<Location> workLocations;
	private int distansCloseToWork = 100;
	public boolean isWorking = false;
	private LocationManager locationManager;
	private LocationListener listenerCoarse;
	private LocationListener listenerFine;
	public boolean locationAvailable = true;
	private final Handler mHandler = new Handler();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		registerLocationListeners();
		mHandler.post(mUpdateText);

		Spinner s = (Spinner) findViewById(R.id.spinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.Project, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);

        //Intent intent = new Intent(getApplicationContext(), CommTest.class);
        //startActivity(intent);
	
	}

	public void  onClickTestComm(View v) {
	    Intent intent = new Intent(getApplicationContext(), CommTest.class);
        startActivity(intent);
	}
	
	public void onClickViewMonth(View v) {
		Toast.makeText(TimezActivity.this, "Har du trukklat?!?!", Toast.LENGTH_LONG).show();
		Intent intent = new Intent(TimezActivity.this, ViewMonth.class);
		startActivity(intent);
	}

	private final Runnable mUpdateText = new Runnable() {
		public void run() {

			float[] results = new float[3];

			if (workLocations != null) {
				for (Location workLocation : workLocations) {
					Location.distanceBetween(currentLocation.getLatitude(),
							currentLocation.getLongitude(),
							workLocation.getLatitude(),
							workLocation.getLongitude(), results);

					if (results[0] < distansCloseToWork) {
						isWorking = true;
					} else {
						isWorking = false;
					}
				}
			}
			else {
				isWorking = false;	
			}
			mHandler.postDelayed(this, 60000);
		}
	};

	public void saveWorkLocations(View view) {
		Intent myIntent = new Intent(view.getContext(),
				EditWorkLocationsActivity.class);
		myIntent.putExtra("@string/latitude", currentLocation.getLatitude());
		myIntent.putExtra("@string/longitude", currentLocation.getLongitude());
		startActivityForResult(myIntent, 0);
	}

	private void registerLocationListeners() {
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		// Initialize criteria for location providers
		Criteria fine = new Criteria();
		fine.setAltitudeRequired(true);
		fine.setBearingRequired(true);
		fine.setSpeedRequired(false);
		fine.setCostAllowed(true);
		fine.setAccuracy(Criteria.ACCURACY_FINE);

		Criteria coarse = new Criteria();
		coarse.setAccuracy(Criteria.ACCURACY_COARSE);

		fine.setPowerRequirement(Criteria.POWER_HIGH);
		coarse.setPowerRequirement(Criteria.POWER_LOW);

		int GPS_TIMEUPDATE = 1000; // update gps every 1 sec
		int GPS_DISTANCEUPDATE = 5; // update gps every 5 m

		// Get at least something from the device,
		// could be very inaccurate though
		currentLocation = locationManager.getLastKnownLocation(locationManager
				.getBestProvider(fine, true));

		if (listenerFine == null || listenerCoarse == null)
			createLocationListeners();

		// Will keep updating about every 1000 ms until
		// accuracy is about 100 meters to get quick fix.
		locationManager.requestLocationUpdates(
				locationManager.getBestProvider(coarse, true), GPS_TIMEUPDATE,
				100, listenerCoarse);
		// Will keep updating about every 1000 ms until
		// accuracy is about 5 meters to get accurate fix.
		locationManager.requestLocationUpdates(
				locationManager.getBestProvider(fine, true), GPS_TIMEUPDATE,
				GPS_DISTANCEUPDATE, listenerFine);
	}

	/**
	 * Creates LocationListeners
	 */
	private void createLocationListeners() {
		listenerCoarse = new LocationListener() {
			@SuppressWarnings("unused")
			public void onStatusChanged(String provider, int status) {
				switch (status) {
				case LocationProvider.OUT_OF_SERVICE:
				case LocationProvider.TEMPORARILY_UNAVAILABLE:
					locationAvailable = false;
					break;
				case LocationProvider.AVAILABLE:
					locationAvailable = true;
				}
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}

			public void onLocationChanged(Location location) {
				currentLocation = location;
				if (location.getAccuracy() > 1000 && location.hasAccuracy())
					locationManager.removeUpdates(this);
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				// TODO Auto-generated method stub

			}
		};
		listenerFine = new LocationListener() {
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				switch (status) {
				case LocationProvider.OUT_OF_SERVICE:
				case LocationProvider.TEMPORARILY_UNAVAILABLE:
					locationAvailable = false;
					break;
				case LocationProvider.AVAILABLE:
					locationAvailable = true;
				}
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}

			public void onLocationChanged(Location location) {
				currentLocation = location;
				if (location.getAccuracy() > 1000 && location.hasAccuracy())
					locationManager.removeUpdates(this);
			}
		};
	}
}
