package com.sylog.timez;


import java.util.Calendar;

import android.app.Activity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.ArrayAdapter;
import android.widget.ListView;



public class ViewMonth extends Activity {

	//private int selectedMonth;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_month);

		// Set display format
		String inFormat = "E, dd MMM";
		
		// Get todays date!
		Calendar cal = Calendar.getInstance();
		// Date today = cal.getTime();

		// String monthName = cal.getDisplayName(Calendar.MONTH, Calendar.LONG,
		// Locale.US);
		super.setTitle( DateFormat.format( "MMMM yyyy" , cal).toString());
		
		cal.set(Calendar.DAY_OF_MONTH, new Integer(1));
		// Date firstDateInMonth = cal.getTime();

		ListView listView = (ListView) findViewById(R.id.mylist);

		int numDaysInMonth = 27;
		String[] values = new String[numDaysInMonth ];
		
		for(int i = 0 ; i < numDaysInMonth; i++) {
			values[i] = DateFormat.format( inFormat , cal).toString();
			if ( 	Calendar.SATURDAY != cal.get(Calendar.DAY_OF_WEEK) && 
					Calendar.SUNDAY   != cal.get(Calendar.DAY_OF_WEEK)	) 
					values[i] = values[i] + "    - 8h ";
			cal.roll(Calendar.DAY_OF_MONTH, true);
		}
		
		
		//First paramenter - Context
		//Second parameter - Layout for the row
		//Third parameter - ID of the View to which the data is written
		//Forth - the Array of data
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, values);

		//Assign adapter to ListView
		listView.setAdapter(adapter);

	}

}
